# How to run
 * `./mvnw clean package --enable-preview`
 * `docker-compose build && docker-compose up`

# How to test
go to `http://localhost:9000/swagger-ui/`
