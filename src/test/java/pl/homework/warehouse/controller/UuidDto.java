package pl.homework.warehouse.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UuidDto {

  private String uuid;
}
