package pl.homework.warehouse.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import pl.homework.warehouse.controller.ProductController.Routes;
import pl.homework.warehouse.controller.form.ProductCreateForm;
import pl.homework.warehouse.model.Product;
import pl.homework.warehouse.model.Stock;
import pl.homework.warehouse.repository.ProductRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@AutoConfigureMockMvc
@ActiveProfiles("disabled-security")
class ProductControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void whenCreateProductIsRequested_newProductShouldBeCreatedWithNewStockEntity() throws Exception {

    ProductCreateForm productCreateForm = new ProductCreateForm("product 1", "description");

    ResultActions resultActions = mockMvc
        .perform(post(Routes.ROOT).contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(productCreateForm)))
        .andExpect(status().isCreated());

    String responseAsString = resultActions.andReturn().getResponse().getContentAsString();
    UuidDto uuidDto = objectMapper.readValue(responseAsString, UuidDto.class);

    Optional<Product> productEntity = productRepository
        .findByUuid(UUID.fromString(uuidDto.getUuid()));
    assertThat(productEntity).isNotEmpty();
    assertThat(productEntity.get().getName()).isEqualTo("product 1");

    assertThat(productEntity.get().getStock().getQuantity()).isEqualTo(0);
    assertThat(productEntity.get().getStock().getProduct().getName()).isEqualTo("product 1");


  }

  @Test
  void whenNoNameIsProvided_shouldResponseWithBadRequest() throws Exception {

    ProductCreateForm productCreateForm = new ProductCreateForm("", "description");

    mockMvc
        .perform(post(Routes.ROOT).contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(productCreateForm)))
        .andExpect(status().isBadRequest());
  }


  @Test
  void whenNonexistentProductIsRequested_shouldRespondWith404() throws Exception {

    Product product = new Product("p1", "d1");
    product.addStock(new Stock(10));
    productRepository.save(product);

    mockMvc
        .perform(get(Routes.ROOT + "/" + UUID.randomUUID()).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());

  }

}
