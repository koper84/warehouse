package pl.homework.warehouse.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import pl.homework.warehouse.controller.dto.StockDetailsDto;
import pl.homework.warehouse.controller.form.StockUpdateForm;
import pl.homework.warehouse.model.Product;
import pl.homework.warehouse.model.Stock;
import pl.homework.warehouse.repository.ProductRepository;

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("disabled-security")
public class StockControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private ObjectMapper objectMapper;


  @Test
  void whenStockForProductIsRequest_shouldReturnCorrectEntity() throws Exception {

    Product product = createProductWithStockInformation();

    UUID productUuid = product.getUuid();
    productRepository.save(product);

    ResultActions resultActions = mockMvc
        .perform(
            get("/products/" + productUuid + "/stocks").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    String responseAsString = resultActions.andReturn().getResponse().getContentAsString();
    StockDetailsDto detailsDto = objectMapper.readValue(responseAsString, StockDetailsDto.class);
    assertThat(detailsDto.getProductUuid()).isEqualTo(productUuid);
    assertThat(detailsDto.getQuantity()).isEqualTo(10);
  }


  @Test
  void whenStockUpdateForProductIsRequested_shouldUpdateStockInformation() throws Exception {
    Product product = createProductWithStockInformation();

    UUID productUuid = product.getUuid();
    productRepository.save(product);

    StockUpdateForm stockUpdateForm = new StockUpdateForm(100);

    mockMvc
        .perform(put("/products/" + productUuid + "/stocks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(stockUpdateForm)))
        .andExpect(status().isOk());

    Optional<Product> updatedProduct = productRepository.findByUuid(productUuid);

    assertThat(updatedProduct).isNotEmpty();
    assertThat(updatedProduct.get().getName()).isEqualTo("p1");
    assertThat(updatedProduct.get().getStock().getQuantity()).isEqualTo(100);

  }

  @Test
  void whenNonexistentStockIsRequested_shouldRespondWith404() throws Exception {
    mockMvc
        .perform(get(StockController.Routes.ROOT + "/" + UUID.randomUUID()))
        .andExpect(status().isNotFound());
  }


  private Product createProductWithStockInformation() {
    Product product = new Product("p1", "d1");
    product.addStock(new Stock(10));
    return product;
  }
}
