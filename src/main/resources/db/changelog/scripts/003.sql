--liquibase formatted sql
--changeset pawel:3


CREATE TABLE "warehouse_user"
(
    id       BIGINT PRIMARY KEY,
    uuid     UUID UNIQUE NOT NULL,
    username varchar(255) DEFAULT NULL,
    password varchar(255) DEFAULT NULL,
    role     varchar(255) DEFAULT NULL

);



INSERT INTO "warehouse_user"
VALUES ((SELECT nextval('hibernate_sequence')), uuid_generate_v4(), 'user',
        crypt('user', gen_salt('bf', 10)), 'ROLE_USER');

--rollback DROP TABLE warehouse_user;

