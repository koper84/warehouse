--liquibase formatted sql
--changeset pawel:1

BEGIN TRANSACTION;

CREATE SEQUENCE hibernate_sequence;


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS pgcrypto;


COMMIT TRANSACTION;

--rollback DROP EXTENSION "uuid-ossp";
--rollback DROP EXTENSION "pgcrypto";
--rollback DROP SEQUENCE hibernate_sequence;
