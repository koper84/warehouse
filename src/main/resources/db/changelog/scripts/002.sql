--liquibase formatted sql
--changeset pawel:2


CREATE TABLE product
(
    id          BIGINT PRIMARY KEY,
    uuid        UUID UNIQUE  NOT NULL,
    name        VARCHAR(255) NOT NULL,
    description TEXT

);

CREATE INDEX idx_products_uuid ON product (uuid);


INSERT INTO product
VALUES (1100, uuid_generate_v4(), 'product 1', 'description 1');

CREATE TABLE stock
(
    id         BIGINT PRIMARY KEY,
    uuid       UUID UNIQUE NOT NULL,
    quantity   INTEGER     NOT NULL,
    product_id BIGINT
        CONSTRAINT fk_stock_product_id REFERENCES product (id) ON DELETE CASCADE


);

INSERT INTO stock
VALUES ((SELECT nextval('hibernate_sequence')), uuid_generate_v4(), 1, 1100);


--rollback DROP TABLE products;
--rollback DROP TABLE stock;

