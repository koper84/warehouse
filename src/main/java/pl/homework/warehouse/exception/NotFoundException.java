package pl.homework.warehouse.exception;

import java.util.UUID;

public class NotFoundException extends RuntimeException {

  public NotFoundException(final UUID uuid) {
    super(String.format("No entity with UUID: %s", uuid));
  }

  public NotFoundException(final String message) {
    super(message);
  }


}
