package pl.homework.warehouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import pl.homework.warehouse.controller.dto.StockDetailsDto;
import pl.homework.warehouse.model.Stock;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface StockMapper {

  @Mappings({
      @Mapping(target = "productUuid", source = "product.uuid"),
  })
  StockDetailsDto stockToStockDetailsDto(Stock stock);
}
