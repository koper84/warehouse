package pl.homework.warehouse.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import pl.homework.warehouse.controller.dto.ProductDetailsDto;
import pl.homework.warehouse.controller.form.ProductCreateForm;
import pl.homework.warehouse.model.Product;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface ProductMapper {

  ProductDetailsDto productToProductDetailsDto(Product product);

  Product createFormToProduct(ProductCreateForm productCreateForm);
}
