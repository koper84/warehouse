package pl.homework.warehouse.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Entity
@Table(name = "product")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@FieldNameConstants
public class Product extends AbstractEntity {

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "description", columnDefinition = "text")
  private String description;

  @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "product", cascade = CascadeType.ALL)
  private Stock stock;

  public Product(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public void addStock(Stock stock) {
    this.stock = stock;
    this.stock.setProduct(this);
  }

  @Override
  public String toString() {
    return "Product{" +
        "name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", id=" + id +
        ", uuid=" + uuid +
        "} " + super.toString();
  }

}
