package pl.homework.warehouse.model;


import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "warehouse_user")
@Getter
@Setter
public class User extends AbstractEntity {

  private String username;
  private String password;
  private String role;
}
