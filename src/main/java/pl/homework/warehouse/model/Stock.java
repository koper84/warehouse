package pl.homework.warehouse.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "stock")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class Stock extends AbstractEntity {

  private int quantity;

  @OneToOne(optional = false)
  @JoinColumn(name = "product_id")
  private Product product;

  public Stock(int quantity) {
    this.quantity = quantity;
  }
}
