package pl.homework.warehouse.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.homework.warehouse.controller.dto.ProductDetailsDto;
import pl.homework.warehouse.controller.dto.UuidDto;
import pl.homework.warehouse.controller.form.ProductCreateForm;
import pl.homework.warehouse.exception.NotFoundException;
import pl.homework.warehouse.mapper.ProductMapper;
import pl.homework.warehouse.model.Product;
import pl.homework.warehouse.model.Stock;
import pl.homework.warehouse.repository.ProductRepository;

@Service
@Slf4j
public class ProductService {

  private final ProductRepository productRepository;

  private final ProductMapper productMapper;

  public ProductService(ProductRepository productRepository,
      ProductMapper productMapper) {
    this.productRepository = productRepository;
    this.productMapper = productMapper;
  }

  public ProductDetailsDto getProduct(final UUID productUUID) {
    return productRepository.findByUuid(productUUID)
        .map(productMapper::productToProductDetailsDto)
        .orElseThrow(() -> new NotFoundException(productUUID));
  }

  public List<ProductDetailsDto> getProducts() {
    return productRepository
        .findAllJoinStock().stream()
        .map(productMapper::productToProductDetailsDto)
        .collect(
            Collectors.toList());
  }

  @Transactional
  public UuidDto createProduct(ProductCreateForm productCreateForm) {
    Product product = productMapper.createFormToProduct(productCreateForm);
    product.addStock(new Stock(0));
    productRepository.save(product);
    return new UuidDto(product.getUuid());
  }

  public void deleteProduct(UUID productUUID) {
    Product product = productRepository.findByUuid(productUUID)
        .orElseThrow(() -> new NotFoundException(productUUID));
    productRepository.delete(product);
  }
}
