package pl.homework.warehouse.service;


import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.homework.warehouse.controller.dto.StockDetailsDto;
import pl.homework.warehouse.controller.form.StockUpdateForm;
import pl.homework.warehouse.exception.NotFoundException;
import pl.homework.warehouse.mapper.StockMapper;
import pl.homework.warehouse.model.Stock;
import pl.homework.warehouse.repository.StockRepository;

@Service
@Slf4j
public class StockService {

  private final StockRepository stockRepository;

  private final StockMapper stockMapper;

  public StockService(StockRepository stockRepository,
      StockMapper stockMapper) {
    this.stockRepository = stockRepository;
    this.stockMapper = stockMapper;
  }

  @Transactional
  public void updateStockByProductUuid(final UUID productUuid,
      StockUpdateForm stockUpdateForm) {
    Stock stock = stockRepository.findByProductUuid(productUuid)
        .orElseThrow(() -> new NotFoundException(productUuid));
    stock.setQuantity(stockUpdateForm.getQuantity());
  }


  public List<StockDetailsDto> getStocks() {
    return stockRepository
        .findAllJoinProduct().stream()
        .map(stockMapper::stockToStockDetailsDto)
        .collect(
            Collectors.toList());
  }

  public StockDetailsDto getStockByProductUuid(UUID productUuid) {
    return stockRepository.findByProductUuid(productUuid).
        map(stockMapper::stockToStockDetailsDto)
        .orElseThrow(() -> new NotFoundException(productUuid));
  }
}
