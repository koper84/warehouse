package pl.homework.warehouse.controller;

import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.homework.warehouse.controller.dto.ProductDetailsDto;
import pl.homework.warehouse.controller.dto.UuidDto;
import pl.homework.warehouse.controller.form.ProductCreateForm;
import pl.homework.warehouse.service.ProductService;

@RestController
@RequestMapping
public class ProductController {

  interface Routes {

    String ROOT = "/products";
    String PRODUCT_BY_UUID = ROOT + "/{productUuid}";
  }

  private final ProductService productService;

  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @PostMapping(Routes.ROOT)
  @ResponseStatus(HttpStatus.CREATED)
  public UuidDto createProduct(@RequestBody @Valid ProductCreateForm productCreateForm) {
    return productService.createProduct(productCreateForm);
  }

  @GetMapping(Routes.PRODUCT_BY_UUID)
  public ProductDetailsDto getProduct(@PathVariable(name = "productUuid") final UUID productUUID) {
    return productService.getProduct(productUUID);
  }

  @GetMapping(Routes.ROOT)
  public List<ProductDetailsDto> getProducts() {
    return productService.getProducts();
  }


  @DeleteMapping(Routes.PRODUCT_BY_UUID)
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteProduct(
      @PathVariable(name = "productUuid") final UUID productUUID) {
    productService.deleteProduct(productUUID);
  }

}
