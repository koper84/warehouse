package pl.homework.warehouse.controller.form;

import javax.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockUpdateForm {
  @PositiveOrZero
  private Integer quantity;
}
