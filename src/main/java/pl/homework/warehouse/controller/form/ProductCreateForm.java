package pl.homework.warehouse.controller.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCreateForm {
  @NotBlank
  @Size(max = 255)
  private String name;
  private String description;
}
