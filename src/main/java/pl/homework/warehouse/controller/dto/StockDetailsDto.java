package pl.homework.warehouse.controller.dto;


import java.util.UUID;
import lombok.Data;

@Data
public class StockDetailsDto {

  private UUID uuid;
  private UUID productUuid;
  private int quantity;

}
