package pl.homework.warehouse.controller.dto;


import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UuidDto {
  private UUID uuid;

}
