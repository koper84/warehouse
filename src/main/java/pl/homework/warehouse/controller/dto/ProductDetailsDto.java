package pl.homework.warehouse.controller.dto;

import java.util.UUID;
import lombok.Data;

@Data
public class ProductDetailsDto {

  private UUID uuid;
  private String name;
  private String description;

}
