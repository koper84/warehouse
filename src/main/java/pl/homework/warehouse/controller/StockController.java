package pl.homework.warehouse.controller;

import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.homework.warehouse.controller.dto.StockDetailsDto;
import pl.homework.warehouse.controller.form.StockUpdateForm;
import pl.homework.warehouse.service.StockService;

@RestController
@RequestMapping
public class StockController {

  interface Routes {

    String ROOT = "/stocks";
    String STOCK_BY_UUID = ROOT + "/{stockUuid}";
    String STOCK_BY_PRODUCT_UUID = ProductController.Routes.PRODUCT_BY_UUID + "/stocks";
  }

  private final StockService stockService;

  public StockController(StockService stockService) {
    this.stockService = stockService;
  }

  @GetMapping(Routes.ROOT)
  public List<StockDetailsDto> getStocks() {
    return stockService.getStocks();
  }

  @GetMapping(Routes.STOCK_BY_PRODUCT_UUID)
  public StockDetailsDto getStockByProductUuid(@PathVariable(name = "productUuid")  UUID productUuid) {
    return stockService.getStockByProductUuid(productUuid);
  }

  @PutMapping(Routes.STOCK_BY_PRODUCT_UUID)
  public void updateStockByProductUuid(@PathVariable(name = "productUuid") UUID productUuid,
      @Valid @RequestBody final StockUpdateForm stockUpdateForm) {
    stockService.updateStockByProductUuid(productUuid, stockUpdateForm);
  }

}
