package pl.homework.warehouse.controller.advice;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.homework.warehouse.exception.NotFoundException;

@Slf4j
@RestControllerAdvice
public class ControllerAdvice {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public void defaultExceptionHandler(final Exception exception) {
    log.error(exception.getMessage(), exception);
  }


  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public void notFoundExceptionHandler(final NotFoundException exception) {
    log.warn(exception.getMessage());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ValidationErrorDTO bodyValidationError(final MethodArgumentNotValidException exception) {
    final BindingResult result = exception.getBindingResult();
    final List<FieldError> fieldErrors = result.getFieldErrors();
    log.error("Wrong form data: {}", fieldErrors);
    final ValidationErrorDTO validationErrorDTO = new ValidationErrorDTO();

    for (final FieldError fieldError: fieldErrors) {
      validationErrorDTO.add(fieldError.getField(), fieldError.getDefaultMessage());
    }

    return validationErrorDTO;
  }

}
