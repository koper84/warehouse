package pl.homework.warehouse.controller.advice;

import lombok.Getter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Getter
public class ValidationErrorDTO {

    private MultiValueMap<String, String> fieldErrors = new LinkedMultiValueMap<>();

    public void add(final String fieldName, final String fieldErrorMessage) {
        fieldErrors.add(fieldName, fieldErrorMessage);
    }
}
