package pl.homework.warehouse.repository;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.homework.warehouse.model.Stock;

public interface StockRepository extends JpaRepository<Stock, Long> {

  @Query("SELECT s FROM Stock s LEFT JOIN FETCH s.product WHERE s.product.uuid = :productUuid")
  Optional<Stock> findByProductUuid(final UUID productUuid);

  @Query("SELECT DISTINCT s FROM Stock s LEFT JOIN FETCH s.product")
  List<Stock> findAllJoinProduct();

}
