package pl.homework.warehouse.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.homework.warehouse.model.User;


public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> getUserByUsername(String username);
}
