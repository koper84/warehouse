package pl.homework.warehouse.repository;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.homework.warehouse.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

  @Query("SELECT s FROM Product s LEFT JOIN FETCH s.stock WHERE s.uuid = :productUuid")
  Optional<Product> findByUuid(final UUID productUuid);


  @Query("SELECT s FROM Product s LEFT JOIN FETCH s.stock")
  List<Product> findAllJoinStock();
}
