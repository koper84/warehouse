FROM adoptopenjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar","-Dagentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000" ]
